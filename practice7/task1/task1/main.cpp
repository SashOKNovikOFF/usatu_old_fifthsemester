#include <iostream>

#include <chrono>
#include <thread>
#include <vector>
#include <string>
#include <set>
#include <algorithm>

using namespace std;

class Demo
{
	public:
		void timeDemo()
		{
			auto time1 = std::chrono::steady_clock::now();

			std::this_thread::sleep_for(std::chrono::milliseconds(100));

			auto time2 = std::chrono::steady_clock::now();

			int milliseconds = std::chrono::duration_cast<std::chrono::microseconds>(time2 - time1).count();
			std::cout << milliseconds << "milliseconds" << std::endl;
		};
		void printVectorInfo(std::vector<int> &myVector)
		{
			if (myVector.empty())
				cout << "Vector is empty." << endl;
			else
			{
				cout << "Capacity = " << myVector.capacity() << endl;
				cout << "Size = " << myVector.size() << endl;
				cout << "Last element = " << myVector.back() << endl;
				cout << "----------" << endl;
			}
		}
		void testVector()
		{
			std::vector<int> myVector;
			const int length = 1000;

			for (int i = 0; i < length; i++)
			{
				myVector.push_back(i);
			}

			printVectorInfo(myVector);

			myVector.clear();
			printVectorInfo(myVector);

			myVector.push_back(1);
			myVector.push_back(2);
			myVector.push_back(3);
			myVector[1] = 7;
			printVectorInfo(myVector);

			myVector.shrink_to_fit();
			printVectorInfo(myVector);

			myVector.resize(2);
			printVectorInfo(myVector);
			
			myVector.reserve(1000);
			printVectorInfo(myVector);

			myVector.pop_back();
			myVector.pop_back();
			myVector.pop_back();
			printVectorInfo(myVector);
		}
		void function1()
		{
			auto time1 = std::chrono::steady_clock::now();

			std::vector<int> myVector;
			const int length = 100000000;

			for (int i = 0; i < length; i++)
			{
				myVector.push_back(i);
			}

			auto time2 = std::chrono::steady_clock::now();

			int milliseconds = std::chrono::duration_cast<std::chrono::microseconds>(time2 - time1).count();
			std::cout << milliseconds << " microseconds" << std::endl;
		}
		void function2()
		{
			auto time1 = std::chrono::steady_clock::now();

			std::vector<int> myVector;
			const int length = 100000000;

			myVector.reserve(length);
			for (int i = 0; i < length; i++)
			{
				myVector.push_back(i);
			}

			auto time2 = std::chrono::steady_clock::now();

			int milliseconds = std::chrono::duration_cast<std::chrono::microseconds>(time2 - time1).count();
			std::cout << milliseconds << " microseconds" << std::endl;

		}
		void testIterator()
		{
			std::vector<int> vector = { 1, 2, 3, 4, 5 };

			auto iterator = vector.begin();
			cout << (*iterator) << std::endl;

			iterator++;
			cout << (*iterator) << endl;

			iterator--;
			cout << (*iterator) << endl;

			cout << (iterator == vector.begin()) << endl;

			cout << (vector.end() - iterator) << endl;
		}
		template <class T> void printVector(std::vector<T> &vector)
		{
			for (unsigned int i = 0; i < vector.size(); i++)
			{
				cout << "v[" << i << "] = " << vector[i] << " | ";
				cout << vector.at(i) << " || ";
			}
			cout << endl;

			std::vector<T>::const_iterator iterator = vector.begin();
			while (iterator != vector.end())
			{
				cout << *iterator << " ";
				iterator++;
			}
			cout << endl;

			for (auto iterator = vector.begin(); iterator != vector.end(); iterator++)
				cout << *iterator << " ";
			cout << endl;

			for (T temp : vector)
				cout << temp << " ";
			cout << endl;

			cout << "Reverse order: " << endl;
			for (auto iterator = vector.rbegin(); iterator != vector.rend(); iterator++)
				cout << *iterator << " ";
			cout << endl;
		}
		void testConst1(std::vector<int> const &vector)
		{
			std::vector<int> temp;
			//vector[1] = 10;
			int x = vector.at(1);
			x = x + vector[2];
			//int& y = vector[2];
			auto iterator = vector.begin();
			//*iterator = 1;
		}
		static void show(std::string s)
		{
			cout << "--" << s;
		}
		void vectorAlgorithms()
		{
			std::vector<std::string> vector = { "cat", "dog", "abyrvalg", "mouse" };
			printVector(vector);
			
			vector.insert(vector.end(), "cat");

			printVector(vector);
			std::set<std::string> sets;
			std::sort(vector.begin(), vector.end(),
				[](const string& a, const string& b) -> bool
			{
				if (a.length() < b.length())
					return true;
				return false;
			});
			std::for_each(vector.begin(), vector.end(), &Demo::show);
			std::for_each(vector.begin(), vector.end(), [](const string &z) -> void { cout << z; });
			
			auto iterator = std::find(vector.begin(), vector.end(), "cat");
			cout << "\nFounded element: " << *iterator << " " << endl;
			
			std::copy(vector.begin(), vector.end(), std::inserter(sets, sets.begin()));
			for (string x : sets)
				cout << x << " ";
			//printVector(vector);
		}

		int temp = 1;
		vector<int> threeW;
		static int number;
		void testConst2(std::vector<int> &vector) const
		{
			std::vector<int> temp;
			//temp = 2;
			//testConst1(vector);
			threeW.at(1);
			number = 0;
			vector[1] = 10;
			int x = vector.at(1);
			x = x + vector[2];
			int& y = vector[2];
			auto iterator = vector.begin();
			*iterator = 1;
		}
};

int main()
{
	Demo demo;
	std::vector<int> vector = { 3, 4, 5, 1, 2 };
	demo.testConst1(vector);
	demo.vectorAlgorithms();

	cin.get();
	return 0;
}