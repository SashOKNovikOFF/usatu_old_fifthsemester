﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class FrogBoard
    {
        int[,] board;
        List<int[,]> boardSet;
        int length;
        int frogs;

        int tempFrog;
        int currentFrog;
        int coordX, coordY;


        public bool PrintFlag { set; get; }
        public bool SaveFlag { set; get; }

        public FrogBoard()
        {
            length = 2;
            frogs = 1;
            board = new int[length + 1, length + 1];
            boardSet = new List<int[,]>();
        }
        public FrogBoard(int length, int frogs)
        {
            if (length <= 1)
                throw new Exception("Board length <= 1!");
            if (frogs <= 0)
                throw new Exception("Frogs number <= 0");
            if (frogs > (length * (length / 2 + length % 2)))
                throw new Exception("So many frogs!");

            this.length = length;
            this.frogs = frogs;
            board = new int[length + 1, length + 1];
            boardSet = new List<int[,]>();
        }

        public void GenereateFrogs()
        {
            ResetBoard();
            Generate();
        }
        public int GetFrogCount()
        {
            if (!SaveFlag)
                throw new Exception("Save off!");
            return boardSet.Count;
        }
        void ResetBoard()
        {
            tempFrog = frogs;
            currentFrog = 0;
            coordX = coordY = 1;

            for (int i = 0; i < length + 1; i++)
                for (int j = 0; j < length + 1; j++)
                    board[i, j] = 0;
        }
        void Generate()
        {
            if (tempFrog == 0)
            {
                if (PrintFlag)
                    PrintBoard();
                if (SaveFlag)
                    boardSet.Add((int[,])board.Clone());
            }
            else if (coordX == (length + 1))
                return;
            else
            {
                currentFrog = 0;

                currentFrog = board[coordY - 1, coordX - 1] + board[coordY - 1, coordX] + board[coordY, coordX - 1];
                if (coordX < length)
                    if (currentFrog < (board[coordY - 1, coordX] + board[coordY - 1, coordX + 1]))
                        currentFrog = board[coordY - 1, coordX] + board[coordY - 1, coordX + 1];

                if (currentFrog < 2)
                {
                    board[coordY, coordX] = 1;
                    tempFrog--;

                    if (coordX < length)
                        coordX++;
                    else if (coordY < length)
                    {
                        coordY++;
                        coordX = 1;
                    }
                    else
                        coordX = length + 1;

                    Generate();
                    tempFrog++;

                    if (coordX > 1)
                        coordX--;
                    else if (coordY > 1)
                    {
                        coordY--;
                        coordX = length;
                    }
                }

                board[coordY, coordX] = 0;

                if (coordX < length)
                    coordX++;
                else if (coordY < length)
                {
                    coordY++;
                    coordX = 1;
                }
                else
                    coordX = length + 1;

                Generate();

                if (coordX > 1)
                    coordX--;
                else if (coordY > 1)
                {
                    coordY--;
                    coordX = length;
                }
            }
        }
        void PrintBoard()
        {
            Console.WriteLine("Board: ");
            for (int i = 1; i < length + 1; i++)
            {
                for (int j = 1; j < length + 1; j++)
                    Console.Write("{0}", board[i, j]);
                Console.WriteLine();
            }
        }
    }
}
