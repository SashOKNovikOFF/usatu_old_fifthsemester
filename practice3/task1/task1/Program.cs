﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FrogBoard board = new FrogBoard(3, 4);
                board.PrintFlag = true;
                board.SaveFlag = true;
                board.GenereateFrogs();
                Console.WriteLine("Subsequence count - {0}", board.GetFrogCount());

                Console.ReadLine();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error: {0}", exc.Message);

                Console.ReadLine();
            }
        }
    }
}
