﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Matrices bestOperands = new Matrices(5);

                Console.WriteLine("The best operands (direct) - {0}", bestOperands.getSolution());
                Console.WriteLine("The best operands (recursive) - {0}", bestOperands.getRecursive(1, 5));

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.WriteLine("Target function: {0}", e.TargetSite);
                Console.ReadLine();
            }
        }
    }
}