﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;


namespace task5
{
    class Matrices
    {
        int[,] allLines;
        int numberMatrices;
        Sizes[] matrices;
        string fileName = "input.txt";
        const int maxRandomNumber = 10;

	    public Matrices()
        {
            numberMatrices = 1;
            allLines = new int[numberMatrices + 1, numberMatrices + 1];
            matrices = new Sizes[numberMatrices + 1];

            CreateRandomFile();
            ReadFile();
        }
        public Matrices(int numberMatrices)
        {
            if (numberMatrices <= 0)
                throw new Exception("Number of matrices < 0!");

            this.numberMatrices = numberMatrices;
            allLines = new int[numberMatrices + 1, numberMatrices + 1];

            CreateRandomFile();
            ReadFile();
        }
        public Matrices(string fileName)
        {
            if (File.Exists("/" + fileName))
                throw new Exception("Chosen file is not exist!");
            
            this.fileName = fileName;

            ReadFile();
        }

        void Reset()
        {
        for (int i = 0; i <= numberMatrices; i++)
            for (int j = i; j <= numberMatrices; j++)
                allLines[i, j] = 0;
        }
        public int getSolution()
        {
            Reset();

            int second;
            int middle;
            int temp = 1;

            while (temp != numberMatrices)
            {
                for (int first = 1; first <= numberMatrices - temp; first++)
                {
                    second = first + temp;
                    middle = first;
                    allLines[first, second] = allLines[first, middle] + allLines[middle + 1, second] + matrices[first].Height * matrices[middle].Length * matrices[second].Length;
                    for (middle += 1; middle < second; middle++)
                        allLines[first, second] = Math.Min(allLines[first, second], allLines[first, middle] + allLines[middle + 1, second] + matrices[first].Height * matrices[middle].Length * matrices[second].Length);
                }

                temp++;
            }

            return allLines[1, numberMatrices];
        }
        public int getRecursive(int first, int second)
        {
            if ((first <= 0) || (first > second) || (first > numberMatrices))
                throw new Exception("Array border error!");
            if ((second <= 0) || (second > numberMatrices))
                throw new Exception("Array border error!");

            Reset();
            return getAnswer(first, second);
        }
        public int getAnswer(int first, int second)
        {
            if (first == second)
                return 0;
            else if (allLines[first, second] != 0)
                return allLines[first, second];
            else
            {
                allLines[first, second] = int.MaxValue;
                for (int middle = first; middle < second; middle++)
                {
                    allLines[first, middle] = getAnswer(first, middle);
                    allLines[middle + 1, second] = getAnswer(middle + 1, second);
                    allLines[first, second] = Math.Min(allLines[first, second], allLines[first, middle] + allLines[middle + 1, second] + matrices[first].Height * matrices[middle].Length * matrices[second].Length);
                }
            }

            return allLines[first, second];
        }

        void CreateRandomFile()
        {
            Random range = new Random();
            using (TextWriter writer = File.CreateText(fileName))
            {
                writer.WriteLine(numberMatrices);
                for (int i = 0; i < numberMatrices; i++)
                    writer.Write("{0} ", range.Next(1, maxRandomNumber));
                writer.Write("{0}", range.Next(1, maxRandomNumber));
            }
        }
        void ReadFile()
        {
            using (TextReader reader = File.OpenText(fileName))
            {
                string line;
                string[] bits;
                int temp;

                line = reader.ReadLine();
                if (!int.TryParse(line, out numberMatrices))
                    throw new Exception("Bad value!");

                allLines = new int[numberMatrices + 1, numberMatrices + 1];
                matrices = new Sizes[numberMatrices + 1];
                for (int i = 0; i < matrices.Length; i++)
                    matrices[i] = new Sizes();


                while ((line = reader.ReadLine()) != null)
                {
                    bits = line.Split(' ');

                    if (!(((numberMatrices % 2) == 0) && (bits.Length == (numberMatrices + 1)) || (bits.Length == (numberMatrices + 1))))
                        throw new Exception("Bad input!");

                    for (int i = 0; i < bits.Length - 1; i++)
                    {
                        if (!int.TryParse(bits[i], out temp))
                            throw new Exception("Bad value!");
                        matrices[i + 1].Height = temp;
                        if (!int.TryParse(bits[i + 1], out temp))
                            throw new Exception("Bad value!");
                        matrices[i + 1].Length = temp;
                    }
                }
            }
        }
    }
}
