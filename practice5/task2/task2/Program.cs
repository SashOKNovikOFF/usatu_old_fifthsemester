﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task2
{
    class Program
    {
        static int GetSum()
        {
            int lengthString;
            int countB;

            EnterData(out lengthString, out countB);
            return CountSum(ref lengthString, ref countB);
        }
        static void EnterData(out int lengthString, out int countB)
        {
            Console.Write("Введите числа N (длина строки) и M (количество букв B подряд) через пробел [N, M]: ");
            string outputString = Console.ReadLine();
            
            string[] bits = outputString.Split(' ');
            
            if (bits.Length != 2)
                throw new Exception("Bad arguments!");
            if (!int.TryParse(bits[0], out lengthString) || !int.TryParse(bits[1], out countB))
                throw new Exception("Bad value!");
            if (lengthString <= 0)
                throw new Exception("Length of the string <= 0!");
            if (countB <= 0)
                throw new Exception("Numbers of B <= 0!");
            if (lengthString == 0)
                throw new Exception("Length of string must be > 0!");
        }
        static int CountSum(ref int lengthString, ref int countB)
        {
            int[] tempArray = new int[lengthString];
            int count = 1;
            for (int i = 0; i < lengthString; i++)
            {
                for (int j = 0; j <= count; j++)
                    if ((i - (j + 1)) >= 0)
                        tempArray[i] = tempArray[i] + tempArray[i - (j + 1)];
                    else
                        tempArray[i]++;

                if (count < countB)
                    count++;
            }

            return tempArray[lengthString - 1];
        }
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Sum: {0}", GetSum());

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.WriteLine("Error: {0}", e.TargetSite);

                Console.ReadLine();
            }
        }
    }
}
