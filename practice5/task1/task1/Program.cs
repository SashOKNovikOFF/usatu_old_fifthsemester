﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Simple dynamic programming.");
                Console.WriteLine("Variant 12. \n");

                int number = 530;
                Console.WriteLine("Direct order counting: {0}", Exercises.CountDirectOrder(number));
                Console.WriteLine("Reverse order counting: {0}", Exercises.CountReverseOrder(number));
                Console.WriteLine("Lazy dynamics order counting: {0}\n", Exercises.CountLazyDynamics(number));
                Exercises.PrintSolution(number);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.WriteLine("Site: {0}", e.TargetSite);

                Console.ReadLine();
            }
        }
    }
}
