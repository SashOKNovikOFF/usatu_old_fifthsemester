﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task1
{
    class Exercises
    {
        static int[] array;
        static int[] previous;

        public static void PrintSolution(int number)
        {
            if (number < 1)
                throw new Exception("The chosen number should be natural number!");
            if (previous == null)
                throw new Exception("You should generate solution before printing!");

            Console.WriteLine("The best solution from {0}:", number);

            int temp = number;
            int summator = 1;
            while (temp != 0)
            {
                for (int i = 0; i < summator; i++)
                    Console.Write("---");
                Console.WriteLine(" {0}", temp);
                temp = previous[temp];
                summator++;
            }
        }
        public static int CountDirectOrder(int number)
        {
            if (number < 1)
                throw new Exception("The chosen number should be natural number!");

            array = new int[number + 1];
            array[1] = 0;

            previous = new int[number + 1];
            previous[1] = 0;

            for (int i = 2; i < number + 1; i++)
            {
                int temp = (int)Math.Round(Math.Sqrt(i));
                if (temp*temp == i)
                {
                    array[i] = array[temp] + 1;
                    previous[i] = temp;
                }
                else
                {
                    array[i] = array[i - 1] + 1;
                    previous[i] = i - 1;
                }
            }

            return array[number];
        }
        public static int CountReverseOrder(int number)
        {
            if (number < 1)
                throw new Exception("The chosen number should be natural number!");

            array = new int[number + 1];
            for (int i = 2; i < number + 1; i++)
                array[i] = int.MaxValue - 1;
            array[1] = 0;
            array[2] = 1;

            previous = new int[number + 1];
            previous[1] = 0;
            previous[2] = 1;

            for (int i = 2; i < number; i++)
            {
                array[i + 1] = Math.Min(array[i + 1], array[i] + 1);
                if (array[i + 1] == (array[i] + 1))
                    previous[i + 1] = i;

                int power = i*i;
                if (power < (number + 1))
                {
                    array[power] = Math.Min(array[power], array[i] + 1);
                    if (array[power] == (array[i] + 1))
                        previous[power] = i;
                }
            }

            return array[number];
        }
        public static int CountLazyDynamics(int number)
        {
            if (number < 1)
                throw new Exception("The chosen number should be natural number!");

            array = new int[number + 1];
            for (int i = 2; i < number + 1; i++)
                array[i] = int.MaxValue;
            array[1] = 0;
            array[2] = 1;

            previous = new int[number + 1];
            previous[1] = 0;
            previous[2] = 1;

            return GetNumber(number);
        }
        static int GetNumber(int number)
        {
            if (array[number] != int.MaxValue)
                return array[number];

            int temp = (int)Math.Round(Math.Sqrt(number));
            if (temp*temp == number)
            {
                array[number] = 1 + Math.Min(GetNumber(number - 1), GetNumber(temp));
                if (array[number] == GetNumber(number - 1))
                    previous[number] = number - 1;
                else
                    previous[number] = temp;
            }
            else
            {
                array[number] = 1 + GetNumber(number - 1);
                previous[number] = number - 1;
            }

            return array[number];
        }
    }
}
