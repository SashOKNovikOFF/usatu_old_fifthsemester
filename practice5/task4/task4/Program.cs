﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Knapsack packet = new Knapsack("input.txt");
                packet.getKnapsack();
                packet.PrintKnapsack();
                
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.WriteLine("Function: {0}", e.TargetSite);
                Console.ReadLine();
            }
        }
    }
}
