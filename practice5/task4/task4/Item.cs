﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    class Item
    {
        public int Price { set; get; }
        public int Weight { set; get; }

        public Item()
        {
            Random random = new Random();
            Price = random.Next(0, int.MaxValue);
            Weight = random.Next(1, int.MaxValue);
        }
        public Item(int price, int weight)
        {
            if (price < 0)
                throw new Exception("Price of item < 0!");
            if (weight < 0)
                throw new Exception("Weihgt of item < 0!");

            this.Price = price;
            this.Weight = weight;
        }

        public int getPrice()
        {
            return Price;
        }
        public int getWeight()
        {
            return Weight;
        }
    }
}
