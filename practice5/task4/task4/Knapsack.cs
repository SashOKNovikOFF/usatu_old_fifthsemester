﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace task4
{
    public class Knapsack
    {
        string fileName = "input.txt";
        int[,] allVariants;
        Item[] items;
        int numberItems;
        int maxWeight;
        List<Item> knapsack;
        const int randomNumber = 25;

	    public Knapsack()
	    {
            numberItems = 1;
            maxWeight = 1;
            knapsack = new List<Item>();
            items = new Item[numberItems + 1];
            allVariants = new int[numberItems + 1, maxWeight + 1];

            CreateRandomFile();
            ReadFile();
	    }
        public Knapsack(int numberItems, int maxWeight)
        {
            if (numberItems < 0)
                throw new Exception("Numbers of items < 0!");
            if (maxWeight < 0)
                throw new Exception("Maximum weight < 0!");

            this.numberItems = numberItems;
            this.maxWeight = maxWeight;
            knapsack = new List<Item>();
            items = new Item[numberItems + 1];
            allVariants = new int[numberItems + 1, maxWeight + 1];

            CreateRandomFile();
            ReadFile();
        }
        public Knapsack(string fileName)
        {
            if (File.Exists("/" + fileName))
                throw new Exception("Chosen file is not exist!");

            this.fileName = fileName;
            knapsack = new List<Item>();
            ReadFile();
        }

        void Reset()
        {
            knapsack.Clear();
            for (int i = 0; i <= numberItems; i++)
                for (int j = 0; j <= maxWeight; j++)
                    allVariants[i, j] = 0;
        }
        public void getKnapsack()
        {
            Reset();
            getSolution();
            findKnapsack(numberItems, maxWeight);
        }
        void getSolution()
        {
            for (int i = 0; i < items.Length; i++)
                allVariants[i, 0] = 0;
            for (int i = 0; i <= maxWeight; i++)
                allVariants[0, i] = 0;

            for (int i = 1; i < items.Length; i++)
                for (int j = 1; j <= maxWeight; j++)
                    if (j >= items[i].Weight)
                        allVariants[i, j] = Math.Max(allVariants[i - 1, j], allVariants[i - 1, j - items[i].Weight] + items[i].Price);
                    else
                        allVariants[i, j] = allVariants[i - 1, j];
        }
        void findKnapsack(int numberItems, int weight)
        {
            if (allVariants[numberItems, weight] == 0)
                return;
            if (allVariants[numberItems - 1, weight] == allVariants[numberItems, weight])
                findKnapsack(numberItems - 1, weight);
            else
            {
                findKnapsack(numberItems - 1, weight - items[numberItems].Weight);
                knapsack.Add(items[numberItems]);
            }
        }
        public void PrintKnapsack()
        {
            if (knapsack.Count == 0)
            {
                Console.WriteLine("Knapsack is empty!");
                return;
            }

            Console.Write("Knapsack:");
            for (int i = 0; i < knapsack.Count; i++)
                Console.Write(" ({0}, {1})", knapsack[i].Price, knapsack[i].Weight);
        }

        void CreateRandomFile()
        {
            Random range = new Random();
            using (TextWriter writer = File.CreateText(fileName))
            {
                writer.WriteLine("{0}", numberItems);
                writer.WriteLine("{0}", maxWeight);
                
                for (int i = 0; i < numberItems - 1; i++)
                    writer.Write("{0} ", range.Next(1, randomNumber));
                writer.WriteLine("{0}", range.Next(1, randomNumber));

                for (int i = 0; i < numberItems - 1; i++)
                    writer.Write("{0} ", range.Next(1, maxWeight));
                writer.WriteLine("{0}", range.Next(1, maxWeight));
            }
        }
        void ReadFile()
        {
            using (TextReader reader = File.OpenText(fileName))
            {
                string line;
                string[] bits;
                int temp;

                line = reader.ReadLine();
                bits = line.Split(' ');
                if (bits.Length == 0)
                    throw new Exception("Bad input!");
                else
                {
                    if (!int.TryParse(bits[0], out numberItems))
                        throw new Exception("Bad value!");
                }
                items = new Item[numberItems + 1];
                for (int i = 0; i < items.Length; i++)
                    items[i] = new Item();

                line = reader.ReadLine();
                bits = line.Split(' ');
                if (bits.Length == 0)
                    throw new Exception("Bad input!");
                else
                {
                    if (!int.TryParse(bits[0], out maxWeight))
                        throw new Exception("Bad value!");
                }
                allVariants = new int[numberItems + 1, maxWeight + 1];
                
                line = reader.ReadLine();
                bits = line.Split(' ');
                if (bits.Length == 0)
                    throw new Exception("Bad input!");

                for (int i = 0; i < bits.Length; i++)
                {
                    if (!int.TryParse(bits[i], out temp))
                        throw new Exception("Bad value!");
                    if (temp < 0)
                        throw new Exception("Numbers of items < 0!");
                    items[i + 1].Price = temp;
                }

                line = reader.ReadLine();
                bits = line.Split(' ');
                if (bits.Length == 0)
                    throw new Exception("Bad input!");

                for (int i = 0; i < bits.Length; i++)
                {
                    if (!int.TryParse(bits[i], out temp))
                        throw new Exception("Bad value!");
                    if (temp < 0)
                        throw new Exception("Numbers of items < 0!");
                    items[i + 1].Weight = temp;
                }
            }
        }
    }
}
