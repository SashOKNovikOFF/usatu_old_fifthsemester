﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                MaxIncreasingSequence mySeq = new MaxIncreasingSequence("input.txt");

                mySeq.GetSequence(false);
                mySeq.PrintResult();
                
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.ReadLine();
            }
        }
    }
}
