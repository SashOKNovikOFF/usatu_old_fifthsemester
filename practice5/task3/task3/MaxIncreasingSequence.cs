﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace task3
{
    class MaxIncreasingSequence
    {
        const int maxRandomNumber = 20;
        int maxUserLength;
        int maxLength;
        string fileName = "input.txt";
        int[] numbers;
        int[] minSubSeq;
        int[] parents;
        int[] previous;
        public List<int> Result { set; get; }
        
        public MaxIncreasingSequence()
        {
            maxUserLength = 1;
            numbers = new int[maxUserLength];
            minSubSeq = new int[maxUserLength + 1];
            parents = new int[maxUserLength];
            previous = new int[maxUserLength];
            CreateRandomFile();
            ReadFile();
        }
        public MaxIncreasingSequence(int maxUserLength)
        {            
            if (maxUserLength < 0)
                throw new Exception("Numbers of numbers < 0!");

            this.maxUserLength = maxUserLength;
            numbers = new int[maxUserLength];
            minSubSeq = new int[maxUserLength + 1];
            parents = new int[maxUserLength];
            previous = new int[maxUserLength];
            CreateRandomFile();
            ReadFile();
        }
        public MaxIncreasingSequence(string fileName)
        {
            if (File.Exists("/" + fileName))
                throw new Exception("Chosen file is not exist!");

            ReadFile();
        }
        
        void Reset()
        {
            minSubSeq[0] = int.MinValue;
            for (int i = 1; i < minSubSeq.Length; i++)
                minSubSeq[i] = int.MaxValue;
        }
        public void GetSequence(bool flag)
        {
            Reset();
            int index;
            maxLength = 0;

            int addition = 0;
            if (flag == true)
                addition = 1;

            for (int i = 0; i < numbers.Length; i++)
            {
                index = Array.BinarySearch(minSubSeq, numbers[i] + addition);
                if (index < 0)
                    index = ~index;
                if (maxLength < index)
                    maxLength = index;
                minSubSeq[index] = numbers[i];
                parents[index] = i;
                if (index == 1)
                    previous[i] = -1;
                else
                    previous[i] = parents[index - 1];
            }

            Result = new List<int>();
            Result.Add(numbers[parents[maxLength]]);
            index = previous[parents[maxLength]];
            while (index != -1)
            {
                Result.Add(numbers[index]);
                index = previous[index];
            }

            Result.Reverse();
        }
        public void PrintResult()
        {
            Console.Write("List:");
            for (int i = 0; i < Result.Count; i++)
                Console.Write(" {0}", Result[i]);
        }

        void CreateRandomFile()
        {
            Random range = new Random();
            using (TextWriter writer = File.CreateText(fileName))
            {
                for (int i = 0; i < maxUserLength - 1; i++)
                    writer.Write("{0} ", range.Next(maxRandomNumber));
                writer.Write("{0}", range.Next(maxRandomNumber));
            }
        }
        void ReadFile()
        {
            using (TextReader reader = File.OpenText(fileName))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    string[] bits = line.Split(' ');

                    if (bits.Length == 0)
                        throw new Exception("Bad input!");
                    else
                    {
                        maxUserLength = bits.Length;
                        numbers = new int[maxUserLength];
                        minSubSeq = new int[maxUserLength + 1];
                        parents = new int[maxUserLength];
                        previous = new int[maxUserLength];
                    }

                    for (int i = 0; i < bits.Length; i++)
                        if (!int.TryParse(bits[i], out numbers[i]))
                            throw new Exception("Bad value!");
                }
            }
        }
    }
}