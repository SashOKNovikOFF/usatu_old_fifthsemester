﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Knapsack knapsack = new Knapsack(8, 56, 4);
                Stopwatch sw = new Stopwatch();

                knapsack.SortItems(Item.CompareByWeight);
                sw.Start();
                knapsack.GetBestSequence(1);
                sw.Stop();
                Console.WriteLine("Time: {0}", sw.Elapsed);

                knapsack.SortItems(Item.CompareByPrice);
                sw.Restart();
                knapsack.GetBestSequence(1);
                sw.Stop();
                Console.WriteLine("Time: {0}", sw.Elapsed);

                knapsack.SortItems(Item.CompareByPriceKG);
                sw.Restart();
                knapsack.GetBestSequence(1);
                sw.Stop();
                Console.WriteLine("Time: {0}", sw.Elapsed);

                //knapsack.PrintSequences();
                knapsack.PrintResult();
                Console.ReadLine();
            }
            catch(Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
                Console.ReadLine();
            }
        }
    }
}
