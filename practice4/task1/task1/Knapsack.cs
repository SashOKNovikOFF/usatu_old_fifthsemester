﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace task1
{
    class Knapsack
    {
        int numberItems;
        double maxWeight;
        int addCondition;
        const double epsilon = 0.0001;
        const int maxRandomPrice = 1000;
        string fileName = "input.txt";
        Item[] items;
        SortedDictionary<double, uint[]> dict;
        List<uint[]> result;

        public Knapsack()
        {
            numberItems = 1;
            maxWeight = 2.0;
            addCondition = 0;
            items = new Item[numberItems];
            dict = new SortedDictionary<double, uint[]>();
            result = new List<uint[]>();
            CreateRandomFile();
            ReadFile();
        }
        public Knapsack(int numberItems, double maxWeight, int addCondition)
        {
            if ((addCondition < 0) || (addCondition > numberItems))
                throw new Exception("Additional condition is not correct!");
            if (numberItems < 0)
                throw new Exception("Number of items < 0!");
            if (maxWeight < 0)
                throw new Exception("Number of maximum weight < 0!");

            this.numberItems = numberItems;
            this.maxWeight = maxWeight;
            this.addCondition = addCondition;
            items = new Item[numberItems];
            dict = new SortedDictionary<double, uint[]>();
            result = new List<uint[]>();
            CreateRandomFile();
            ReadFile();
        }
        public Knapsack(int numberItems, double maxWeight, int addCondition, string fileName)
        {
            if ((addCondition < 0) || (addCondition > numberItems))
                throw new Exception("Additional condition is not correct!");
            if (numberItems < 0)
                throw new Exception("Number of items < 0!");
            if (maxWeight < 0)
                throw new Exception("Number of maximum weight < 0!");
            if (File.Exists("/" + fileName))
                throw new Exception("Chosen file is not exist!");
            
            this.numberItems = numberItems;
            this.maxWeight = maxWeight;
            this.addCondition = addCondition;
            this.fileName = fileName;
            items = new Item[numberItems];
            dict = new SortedDictionary<double, uint[]>();
            result = new List<uint[]>();
            ReadFile();
        }

        public void SortItems(Comparison<Item> comparision)
        {
            Array.Sort(items, comparision);
        }
        public void GetBestSequence(int mode)
        {
            if ((mode <= 0) || (mode >= 4))
                throw new Exception("Choose mode 1, 2 or 3!");

            ResetData();

            double tempSum;
            for (uint i = 1; i <= numberItems; i++)
            {
                SequenceGenerator generator = new SequenceGenerator(i, (uint)numberItems);
                generator.SaveFlag = true;
                generator.GenerateIncreaseNonRecursive();

                for (int j = generator.SequenceList.Count - 1; j >= 0; j--)
                {
                    tempSum = 0;
                    for (int k = 0; k < generator.SequenceList[j].Length; k++)
                        tempSum += items[generator.SequenceList[j][k] - 1].Weight;
                    if ((tempSum > maxWeight) || (addCondition != 0) && (generator.SequenceList[j].Length >= addCondition))
                        generator.SequenceList.Remove(generator.SequenceList[j]);
                }
                
                for (int j = 0; j < generator.SequenceList.Count; j++)
                {
                    tempSum = 0;
                    for (int k = 0; k < generator.SequenceList[j].Length; k++)
                        tempSum += items[generator.SequenceList[j][k] - 1].Price;
                    dict[tempSum]=generator.SequenceList[j];
                }
            }

            SearchSequence(mode);
        }
        void ResetData()
        {
            dict.Clear();
            result.Clear();
        }

        void SearchSequence(int mode)
        {
            switch (mode)
            {
                case 1: SearchBestVariants();
                        break;
                case 2: Search10BestVariants();
                        break;
                case 3: Search09MaxVariants();
                        break;
            }
        }
        void SearchBestVariants()
        {
            double maxPrice = dict.Keys.Max();
            foreach (double price in dict.Keys)
                if (Math.Abs(maxPrice - price) < epsilon)
                    result.Add(dict[price]);
        }
        void Search10BestVariants()
        {
            while (dict.Keys.Count > 10)
                dict.Remove(dict.Keys.Min());
            foreach (double price in dict.Keys)
                result.Add(dict[price]);
        }
        void Search09MaxVariants()
        {
            double maxPrice = dict.Keys.Max();
            foreach (double price in dict.Keys)
                if (price > 0.9*maxPrice)
                    result.Add(dict[price]);
        }

        void CreateRandomFile()
        {
            Random range = new Random();
            using (TextWriter writer = File.CreateText(fileName))
            {
                double temp;
                for (int i = 0; i < numberItems; i++)
                {
                    temp = 0.0;
                    while (temp == 0.0)
                        if (maxWeight > 1.0)
                            temp = range.NextDouble() * range.Next((int)maxWeight);
                        else
                            temp = range.NextDouble();
                    writer.Write("{0} ", temp);
                    writer.WriteLine("{0}", range.NextDouble() * range.Next(maxRandomPrice));
                }
            }
        }
        void ReadFile()
        {
            using (TextReader reader = File.OpenText(fileName))
            {
                string line;
                int tempNumber = 0;
                double tempWeight;
                double tempPrice;
                
                while ((line = reader.ReadLine()) != null)
                {
                    string[] bits = line.Split(' ');

                    if (bits.Length != 2)
                        throw new Exception("Bad input!");

                    if (!double.TryParse(bits[0], out tempWeight))
                        throw new Exception("Bad value!");
                    if (!double.TryParse(bits[1], out tempPrice))
                        throw new Exception("Bad value!");

                    items[tempNumber] = new Item(tempWeight, tempPrice);

                    tempNumber++;
                }
                if (tempNumber != numberItems)
                    throw new Exception("Not all items!");
            }
        }
        public void PrintSequences()
        {
            foreach (double price in dict.Keys)
            {
                Console.WriteLine("Sequence:");
                for (int i = 0; i < dict[price].Length; i++)
                {
                    Console.Write("\t P: {0:0.00}, ", items[dict[price][i] - 1].Price);
                    Console.WriteLine("W: {0:0.00}", items[dict[price][i] - 1].Weight);
                }
            }
        }
        public void PrintResult()
        {
            if (result.Count == 0)
                Console.WriteLine("There is no need results.");

            for (int i = 0; i < result.Count; i++)
            {
                Console.WriteLine("Result sequence:");
                for (int j = 0; j < result[i].Length; j++)
                {
                    Console.Write("\t P: {0:0.00}, ", items[result[i][j] - 1].Price);
                    Console.WriteLine("W: {0:0.00}", items[result[i][j] - 1].Weight);
                }
            }
        }
    }
}
