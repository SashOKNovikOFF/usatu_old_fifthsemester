﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task1
{
    class Item
    {
        public double Weight { set; get; }
        public double Price { set; get; }

        public Item()
        {
            Weight = 0.0;
            Price = 0.0;
        }
        public Item(double weight, double price)
        {
            if (weight <= 0.0)
                throw new Exception("Weight of item <= 0.0 kg!");
            if (price < 0.0)
                throw new Exception("Price < 0$!");

            Weight = weight;
            Price = price;
        }

        public static Comparison<Item> CompareByWeight = (i1, i2) => i1.Weight.CompareTo(i2.Weight);
        public static Comparison<Item> CompareByPrice = (i1, i2) => i2.Price.CompareTo(i1.Price);
        public static Comparison<Item> CompareByPriceKG = (i1, i2) => (i1.Price / i1.Weight).CompareTo(i2.Price / i2.Weight);
    }
}
