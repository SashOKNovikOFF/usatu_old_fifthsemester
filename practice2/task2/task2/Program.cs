﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Transposal transpose = new Transposal(4);
                transpose.PrintFlag = true;
                transpose.SaveFlag = true;
                transpose.GenerateTransposal();

                Console.WriteLine("{0}", transpose.CompareTransposal());

                Console.ReadLine();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error: {0}", exc.Message);
                Console.WriteLine("Target Site: {0}", exc.TargetSite);

                Console.ReadLine();
            }
        }
    }
}
