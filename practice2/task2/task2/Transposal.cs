﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Transposal
    {
        int[] resultSeq;
        List<int[]> resultArray;
        uint length;
        public bool PrintFlag { set; get; }
        public bool SaveFlag { set; get; }

        void Swap(ref int first, ref int second)
        {
            int temp;
            temp = second;
            second = first;
            first = temp;
        }
        public bool CompareTransposal()
        {
            if (resultArray.Count == 0)
                throw new Exception("Result array is empty!");

            long factorial = 1;
            for (int i = 1; i <= length; i++)
                factorial *= i;

            if (factorial == (long)resultArray.Count)
                return true;
            else
                return false;
        }

        public Transposal()
        {
            length = 1;
            resultSeq = new int[length];
            resultArray = new List<int[]>();
        }
        public Transposal(uint length)
        {
            if (length == 0)
                throw new Exception("Length = 0!");

            this.length = length;
            resultSeq = new int[length];
            resultArray = new List<int[]>();
        }

        public void GenerateTransposal()
        {
            ResetTransposal();
            GenerateNonRecursive();
        }
        void ResetTransposal()
        {
            for (int i = 0; i < length; i++)
                resultSeq[i] = i + 1;
            resultArray.Clear();
        }
        void PrintTransposal()
        {
            Console.Write("Code: ");
            for (int i = 0; i < length; i++)
                Console.Write("{0}", resultSeq[i]);
            Console.WriteLine();
        }
        void GenerateNonRecursive()
        {
            bool flag;
            int position;
            int tempMinPos = (int)length;

            while(true)
            {
                if (PrintFlag)
                    PrintTransposal();
                if (SaveFlag)
                    resultArray.Add((int[])resultSeq.Clone());

                flag = true;

                for (position = (int)length - 2; position >= 0; position--)
                    if (resultSeq[position] < resultSeq[position + 1])
                    {
                        flag = false;
                        break;
                    }

                if (flag)
                    return;

                for (int i = (int)length - 1; i > position; i--)
                {
                    if (resultSeq[position] < resultSeq[i])
                    {
                        tempMinPos = i;
                        break;
                    }
                }

                Swap(ref resultSeq[position], ref resultSeq[tempMinPos]);
                Array.Reverse(resultSeq, position + 1, (int)length - position - 1);
            }
        }
    }
}
