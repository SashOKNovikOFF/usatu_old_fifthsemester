﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                GrayCode code = new GrayCode(5);
                code.PrintFlag = true;
                code.SaveFlag = true;
                code.GenerateGrayCode();
                Console.WriteLine("{0}", code.CompareGrayCode());

                Console.ReadLine();
            }
            catch(Exception exc)
            {
                Console.WriteLine("Error: {0}", exc.Message);
                Console.WriteLine("Target Site: {0}", exc.TargetSite);

                Console.ReadLine();
            }
        }
    }
}
