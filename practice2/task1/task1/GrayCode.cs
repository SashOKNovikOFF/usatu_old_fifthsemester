﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class GrayCode
    {
        byte[] resultSeq;
        List<byte[]> resultArray;
        uint length;
        uint height;
        uint numberSeq;
        bool flag;
        public bool PrintFlag { set; get; }
        public bool SaveFlag { set; get; }

        public GrayCode()
        {
            length = 1;
            numberSeq = 0;
            height = (uint)Math.Pow(2, length);
            resultSeq = new byte[length];
            resultArray = new List<byte[]>();
        }
        public GrayCode(uint length)
        {
            if (length == 0)
                throw new Exception("Length = 0!");

            this.length = length;
            numberSeq = 0;
            height = (uint)Math.Pow(2, length);
            resultSeq = new byte[length];
            resultArray = new List<byte[]>();
        }

        public bool CompareGrayCode()
        {
            for (int i = 0; i < resultArray.Count; i++)
            {
                byte[] temp = getGrayCode(i);
                //byte[] temp = getGrayCode(getGrayPosition(resultArray[i]));
                for (int j = 0; j < resultArray[i].Length; j++)
                    if (resultArray[i][j] != temp[j])
                        return false;
            }

            return true;
        }

        int BinaryToDecimal(byte[] number)
        {
            int temp = 0;
            uint power = 0;
            for (int i = number.Length - 1; i >= 0; i--)
            {
                temp = temp + (int)(number[i] * Math.Pow(2, power));
                power++;
            }

            return temp;
        }
        byte[] DecimalToBinary(int number)
        {
            uint position = length - 1;
            byte[] result = new byte[length];
            while (number >= 1)
            {
                result[position] = (byte)(number % 2);
                number = number / 2;
                position--;
            }

            return result;
        }
        public byte[] getGrayCode(int number)
        {
            return DecimalToBinary(number ^ (number >> 1));
        }
        public int getGrayPosition(byte[] number)
        {
            int position = 0;
            int gray = BinaryToDecimal(number);

            while (gray != 0)
            {
                position = position ^ gray;
                gray >>= 1;
            }

            return position;
        }

        public void GenerateGrayCode()
        {
            ResetGrayCode();
            GenerateRecursive(0, true);
        }
        void ResetGrayCode()
        {
            numberSeq = 0;
            flag = false;
            for (uint i = 0; i < length; i++)
                resultSeq[i] = (byte)0;
            resultArray.Clear();
        }
        void PrintGrayCode()
        {
            Console.Write("Code: ");
            for (int i = 0; i < length; i++)
                Console.Write("{0}", resultSeq[i]);
            Console.WriteLine();
        }
        void GenerateRecursive(uint position, bool direction)
        {
            if (position == length)
            {
                if (PrintFlag)
                    PrintGrayCode();
                if (SaveFlag)
                    resultArray.Add((byte[])resultSeq.Clone());
                numberSeq++;
                if (numberSeq == height)
                    flag = true;
                return;
            }

            if (flag)
                return;
            resultSeq[position] = direction ? (byte)0 : (byte)1;
            GenerateRecursive(position + 1, true);

            if (flag)
                return;
            resultSeq[position] = direction ? (byte)1 : (byte)0;
            GenerateRecursive(position + 1, false);
        }
    }
}
