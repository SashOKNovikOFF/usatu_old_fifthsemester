﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace task1
{
    class Program
    {
        const uint lengthMax = 30;
        const uint numberMax = 30;
        const uint lengthBorder = 7;
        const uint numberBorder = 7;

        static void PrintSequencesTable()
        {
            Stopwatch watch = new Stopwatch();
            long timeRecursive, timeNonRecursive;

            Console.WriteLine("Table of generating result: ");
            Console.WriteLine("Length | Number | Sequence Number | Recursive Time | NonRecursive Time");
            for (uint length = 1; length <= lengthMax; length++)
                for (uint number = 1; number <= numberMax; number++)
                {
                    SequenceGenerator sequence = new SequenceGenerator(length, number);
                    if ((length < lengthBorder) && (number < numberBorder))
                        sequence.SaveFlag = true;
                    
                    watch.Restart();
                    sequence.GenerateRecursive();
                    watch.Stop();

                    timeRecursive = watch.ElapsedMilliseconds;

                    watch.Restart();
                    sequence.GenerateNonRecursive();
                    watch.Stop();

                    timeNonRecursive = watch.ElapsedMilliseconds;

                    Console.Write("{0} \t {1} \t {2} \t ", length, number, sequence.SequenceList.Count);
                    Console.WriteLine("{0} \t {1}", timeRecursive, timeNonRecursive);
                }
        }
        static void PrintCombinationsTable()
        {
            Stopwatch watch = new Stopwatch();
            long timeRecursive, timeNonRecursive;

            Console.WriteLine("Table of generating result: ");
            Console.WriteLine("Length | Number | Sequence Number | Recursive Time | NonRecursive Time");
            for (uint length = 1; length <= lengthMax; length++)
                for (uint number = length; number <= numberMax; number++)
                {
                    SequenceGenerator sequence = new SequenceGenerator(length, number);
                    if ((length < lengthBorder) && (number < numberBorder))
                        sequence.SaveFlag = true;

                    watch.Restart();
                    sequence.GenerateIncreaseRecursive();
                    watch.Stop();

                    timeRecursive = watch.ElapsedMilliseconds;

                    watch.Restart();
                    sequence.GenerateIncreaseNonRecursive();
                    watch.Stop();

                    timeNonRecursive = watch.ElapsedMilliseconds;

                    Console.Write("{0} \t {1} \t {2} \t ", length, number, sequence.SequenceList.Count);
                    Console.WriteLine("{0} \t {1}", timeRecursive, timeNonRecursive);
                }
        }
        static void PrintEqualsTable()
        {
            Stopwatch watch = new Stopwatch();
            long timeCombination, timeBinary;

            Console.WriteLine("Table of generating result: ");
            Console.WriteLine("Length | Number | Recursive Time | NonRecursive Time");
            for (uint length = 1; length <= lengthMax; length++)
                for (uint number = length; number <= numberMax; number++)
                {
                    SequenceGenerator sequenceCombination = new SequenceGenerator(length, number);
                    SequenceGenerator sequenceBinary = new SequenceGenerator(number, length);

                    watch.Restart();
                    sequenceCombination.GenerateIncreaseRecursive();
                    watch.Stop();

                    timeCombination = watch.ElapsedMilliseconds;

                    watch.Restart();
                    sequenceBinary.GenerateBinary();
                    watch.Stop();

                    timeBinary = watch.ElapsedMilliseconds;

                    Console.Write("{0} \t {1} \t ", length, number);
                    Console.WriteLine("{0} \t {1}", timeCombination, timeBinary);
                }
        }
        static void Main(string[] args)
        {
            try
            {
                PrintEqualsTable();

                Console.ReadLine();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                Console.ReadLine();
            }
        }
    }
}
