﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task1
{
    class SequenceGenerator
    {
        uint[] sequence;
        public List<uint[]> SequenceList { set; get; }
        uint length;
        uint numbers;
        uint numbersOnes;
        int temproraryPosition;
        public bool PrintFlag { set; get; }
        public bool PrintDebug { set; get; }
        public bool SaveFlag { set; get; }

        public SequenceGenerator()
        {
            length = 1;
            numbers = 1;
            temproraryPosition = -1;
            sequence = new uint[length];
            SequenceList = new List<uint[]>();
        }
        public SequenceGenerator(uint length, uint numbers)
        {
            if ((length == 0) && (numbers == 0))
                throw new Exception("Length = 0, Numbers = 0!");
            else if (length == 0)
                throw new Exception("Length = 0!");
            else if (numbers == 0)
                throw new Exception("Numbers = 0!");

            this.length = length;
            this.numbers = numbers;
            temproraryPosition = -1;
            sequence = new uint[length];
            SequenceList = new List<uint[]>();
        }

        void PrintSequence()
        {
            Console.Write("Sequence:");
            for (int i = 0; i < length; i++)
                Console.Write(" {0}", sequence[i]);
            Console.WriteLine();
        }
        void ResetSequence()
        {
            temproraryPosition = -1;
            SequenceList.Clear();
            for (int i = 0; i < length; i++)
                sequence[i] = 1;
        }
        public static bool SequenceEquals(SequenceGenerator sequence1, SequenceGenerator sequence2)
        {
            if (sequence1.SequenceList.Count != sequence2.SequenceList.Count)
                return false;
            if ((sequence1.SaveFlag == false) || (sequence2.SaveFlag == false))
                throw new Exception("Lists are empty!");
            if (sequence1.SequenceList[sequence1.SequenceList.Count - 1][0] != sequence2.SequenceList[sequence2.SequenceList.Count - 1][0])
                return false;

            bool flag;
            for (int i = 0; i < sequence1.SequenceList.Count; i++)
            {
                flag = false;
                for (int j = 0; j < sequence1.SequenceList[i].Length; j++)
                    if (sequence1.SequenceList[i][j] != sequence2.SequenceList[i][j])
                    {
                        flag = true;
                        break;
                    }

                if (flag)
                    return false;
            }

            return true;
        }
        
        public void GenerateRecursive()
        {
            ResetSequence();
            GenRec();
        }
        public void GenerateRecursive(uint startIndex)
        {
            ResetSequence();
            GenRec(startIndex);
        }
        public void GenerateIncreaseRecursive()
        {
            ResetSequence();
            GenIncrRec();
        }
        void GenRec()
        {
            if (temproraryPosition == length - 1)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());
            }
            else
            {
                temproraryPosition++;
                for (uint i = 1; i <= numbers; i++)
                {
                    sequence[temproraryPosition] = i;
                    GenRec();
                }
                temproraryPosition--;
            }
        }
        void GenRec(uint startIndex)
        {
            if (startIndex < 0)
                throw new Exception("Start Index < 0!");
            else if (startIndex == length)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());
            }
            else
            {
                for (uint i = 1; i <= numbers; i++)
                {
                    sequence[startIndex] = i;
                    GenRec(startIndex + 1);
                }
            }
        }
        void GenIncrRec()
        {
            if (length > numbers)
                throw new Exception("Length < Numbers!");

            if (temproraryPosition == length - 1)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());
            }
            else
            {
                temproraryPosition++;
                
                uint i;
                if ((temproraryPosition - 1) < 0)
                    i = sequence[0];
                else
                    i = sequence[temproraryPosition - 1] + 1;
                for (; i <= numbers - (length - (temproraryPosition + 1)); i++)
                {
                    sequence[temproraryPosition] = i;
                    if (PrintDebug)
                    {
                        for (int j = 0; j < temproraryPosition; j++)
                            Console.Write("\t");
                        Console.Write("GenerateIncrease({0}) \t", temproraryPosition);
                        for (int j = 0; j < temproraryPosition; j++)
                            Console.Write("{0}, ", sequence[j]);
                        Console.WriteLine("{0}", sequence[temproraryPosition]);
                    }
                    GenIncrRec();
                }

                temproraryPosition--;
            }
        }

        public void GenerateNonRecursive()
        {
            ResetSequence();

            int position;
            while (true)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());

                position = (int)length - 1;
                while (sequence[position] == numbers)
                {
                    sequence[position] = 1;
                    position--;
                    if (position < 0)
                        return;
                }
                sequence[position]++;
            }
        }
        public void GenerateIncreaseNonRecursive()
        {
            if (length > numbers)
                throw new Exception("Length > Numbers!");

            ResetSequence();
            for (uint i = 0; i < length; i++)
                sequence[i] = i + 1;

            int position;
            while (true)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());

                position = (int)length - 1;
                while (sequence[position] > (numbers - length + position))
                {
                    position--;
                    if (position < 0)
                        return;
                }

                sequence[position]++;
                for (int i = position + 1; i < length; i++)
                    sequence[i] = sequence[i - 1] + 1;
            }
        }
        
        public void GenerateBinary()
        {
            ResetBin();
            GenBin();
        }
        void ResetBin()
        {
            if (length < numbers)
                throw new Exception("Length < Numbers!");
            numbersOnes = 0;
            temproraryPosition = -1;
            for (int i = 0; i < length; i++)
                sequence[i] = 0;
        }
        void GenBin()
        {
            if (temproraryPosition == length - 1)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());
            }
            else
            {
                temproraryPosition++;

                if ((length - (temproraryPosition + 1)) >= (numbers - numbersOnes))
                {
                    sequence[temproraryPosition] = 0;
                    GenBin();
                }

                if (numbersOnes < numbers)
                {
                    sequence[temproraryPosition] = 1;
                    numbersOnes++;
                    GenBin();
                }

                if (sequence[temproraryPosition] == 1)
                    numbersOnes--;
                temproraryPosition--;
            }
        }
    }
}
