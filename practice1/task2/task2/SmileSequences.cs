﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class SmileSequences
    {
        uint[] sequence;
        char[] eyes = { ':', ';', '=', '%', 'В', '8', 'X' };
        char[] noses = { '-', '>', 'n', '^' };
        char[] lipses = { ')', ']', '}', '|', '>', '\\' };
        public List<uint[]> SequenceList { set; get; }
        uint length;
        uint numbersEyes, numbersNoses, numbersLipses;
        public bool PrintFlag { set; get; }
        public bool SaveFlag { set; get; }

        public SmileSequences()
        {
            length = 3;
            numbersEyes = 1;
            numbersNoses = 1;
            numbersLipses = 1;
            sequence = new uint[length];
            SequenceList = new List<uint[]>();
        }
        public SmileSequences(uint numbersEyes, uint numbersNoses, uint numbersLipses)
        {
            if (numbersEyes == 0)
                throw new Exception("There are no eyes!");
            if (numbersLipses == 0)
                throw new Exception("There are no lipses!");

            this.numbersEyes = numbersEyes;
            this.numbersNoses = numbersNoses;
            this.numbersLipses = numbersLipses;
            this.length = numbersEyes + numbersNoses + numbersLipses;
            sequence = new uint[length];
            SequenceList = new List<uint[]>();
        }

        void PrintSequence()
        {
            uint temprorary;
            Console.Write("Smile: ");
            for (int i = 0; i < length; i++)
            {
                if ((sequence[i] >= 0) && (sequence[i] <= 9))
                    temprorary = 0;
                else if ((sequence[i] >= 10) && (sequence[i] <= 99))
                    temprorary = 10;
                else
                    temprorary = 100;
                switch (temprorary)
                {
                    case 0 : Console.Write("{0}", eyes[sequence[i] - temprorary]);
                        break;
                    case 10 : Console.Write("{0}", noses[sequence[i] - temprorary]);
                        break;
                    case 100 : Console.Write("{0}", lipses[sequence[i] - temprorary]);
                        break;
                }
            }
            Console.WriteLine();
        }
        void ResetSequence()
        {
            SequenceList.Clear();

            uint position = 0;
            for (; position < numbersEyes; position++)
                sequence[position] = 0;
            for (; position < numbersEyes + numbersNoses; position++)
                sequence[position] = 10;
            for (; position < length; position++)
                sequence[position] = 100;
        }
        public void GenerateSmiles()
        {
            ResetSequence();

            int position;
            while (true)
            {
                if (PrintFlag)
                    PrintSequence();
                if (SaveFlag)
                    SequenceList.Add((uint[])sequence.Clone());

                position = (int)length - 1;
                while (sequence[position] == 99 + lipses.Length)
                {
                    sequence[position] = 100;
                    position--;
                }
                while ((sequence[position] == 9 + noses.Length) && (noses.Length != 0))
                {
                    sequence[position] = 10;
                    position--;
                }
                while (sequence[position] == -1 + eyes.Length)
                {
                    sequence[position] = 0;
                    position--;
                    if (position < 0)
                        return;
                }
                
                sequence[position]++;
            }
        }
    }
}
