﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SmileSequences sequence = new SmileSequences(2, 1, 1);
                sequence.PrintFlag = true;
                sequence.GenerateSmiles();

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                Console.ReadLine();
            }
        }
    }
}
